package com.university.students.controller;

import com.university.students.model.Student;
import com.university.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.core.Response;

import java.util.List;

@RestController
@RequestMapping("/")
public class StudentsController {

    private final StudentService studentService;

    @Autowired
    public StudentsController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("students")
    public Response retrieveAllStudents(){
     List<Student> students = this.studentService.retrieveAllStudents();
     if(!students.isEmpty()) {
         return Response.ok(students).build();
     }
     else return Response.serverError().build();
    }

    @GetMapping("students/{groupNumber}")
    public Response retrieveAllStudentsByGroupNumber(@PathVariable int groupNumber){
       List<Student> groupNumberstudents = this.studentService.retrieveAllStudentsByGroupNumber(groupNumber);
       if(!groupNumberstudents.isEmpty()) {
            return Response.ok(groupNumberstudents).build();
       }
       else return Response.serverError().build();
    }

    @GetMapping("student/{id}")
    public  Response retrieveStudentById(@PathVariable Long id) {
        Student student = this.studentService.getStudent(id);
        if (student != null) {
            return Response.ok(student).build();
        }
        else return Response.serverError().build();
    }
}
