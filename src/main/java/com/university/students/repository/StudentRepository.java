package com.university.students.repository;

import com.university.students.model.Student;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    List<Student> findStudentsByGroupNumber(int group);

    Student findStudentById(Long id);
}
