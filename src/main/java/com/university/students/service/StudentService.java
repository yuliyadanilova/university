package com.university.students.service;

import com.university.students.model.Student;
import com.university.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    public List<Student> retrieveAllStudents() {
       return (List<Student>) this.studentRepository.findAll();
    }

    public List<Student> retrieveAllStudentsByGroupNumber(int group) {
        return this.studentRepository.findStudentsByGroupNumber(group);
    }

    public Student getStudent(Long id){
        return this.studentRepository.findStudentById(id);
    }
}
